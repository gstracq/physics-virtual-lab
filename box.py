'''
Testing code
'''
from vpython import box, vector, color, sphere, rate, arrow

DELTA = 0.005

def main():
    '''
    main function
    '''

    ball = sphere(pos=vector(-5, 0, 0), radius=0.5, color=color.cyan)
    right_wall = box(pos=vector(6, 0, 0), size=vector(0.2, 12, 12), color=color.green)
    left_wall = box(pos=vector(-6, 0, 0), size=vector(0.2, 12, 12), color=color.green)

    ball.velocity = vector(25, 0 ,0 )

    time_ball = 0

    vscale = 0.1
    varr = arrow(pos=ball.pos, axis=vscale*ball.velocity, color=color.yellow)

    while time_ball < 5:
        rate(100)
        if ball.pos.x > right_wall.pos.x or ball.pos.x < left_wall.pos.x:
            ball.velocity.x = - ball.velocity.x

        ball.pos = ball.pos + ball.velocity*DELTA
        time_ball = time_ball + DELTA



if __name__ == '__main__':
    main()
    